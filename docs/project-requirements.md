# To-Do List Coding Task
## Task Requirements

1. **Layout:**
   - Design a clean and responsive layout that works well on various screen sizes.
   
2. **Input Fields:**
   - Include input fields for adding new tasks.
   - Implement a button to submit new tasks.

3. **Task Display:**
   - Display a list of tasks.
   - Provide options to mark tasks as complete and delete them.

4. **Task Submission:**
   - Add the submitted task to the list when the user submits the form.

5. **User Interaction:**
   - Implement smooth user interactions.
   - Display error messages when users attempt to submit empty tasks.

## Technical Details

- **Frameworks:**
  - You are free to use frameworks, and React is highly encouraged.

- **Persistence:**
  - This is a front-end task, and there is no requirement for server persistence.

- **Submission:**
  - Provide a link to the code repository, ensuring accessibility for code review.

## Bonus Features (Optional)

Feel free to include additional features such as:
- Task filtering and sorting.
- Task reordering.
- Storage persistence (optional).
- **Security:**
  - Implement security measures to prevent cross-site scripting attacks.

## Timeline

Please aim to complete this task by `2024.03.04`. We appreciate your effort and look forward to reviewing your submission. Good luck!