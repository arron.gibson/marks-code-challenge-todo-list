# To-Do List Coding Task

## Overview

This coding task involves creating a to-do list with specific requirements for a clean and responsive layout. The goal is to implement functionality that allows users to add new tasks, mark them as complete, and delete tasks, and receive error messages for incomplete entries.

## Task Requirements

Please review the [Project Requirements Documentation](./docs/project-requirements.md).

## Development

Please review the [Developer Documentation](./docs/development.md).

## Deployment

Simply update the `main` branch to deploy to GitLab pages at:

https://todo.arrongibson.com/